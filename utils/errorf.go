package utils

import (
	"fmt"
	"runtime"
)

// Errorf - return an error with useful logging prefixed
func Errorf(format string, args ...interface{}) error {
	_, file, line, _ := runtime.Caller(1)
	prefix := fmt.Sprintf("%v:%v: ", file, line)
	return fmt.Errorf(prefix+format, args...)
}
