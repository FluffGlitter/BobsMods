package server

import (
	"bytes"
	"html/template"
	"log"
	"net"
	"net/http"

	"gitlab.com/FluffGlitter/BobsMods/client/utils"
)

// Adaptor - http.Handler adaptor
// Should return a function which takes a http.Handler and returns
// another http.Handler
type Adaptor func(http.Handler) http.Handler

// LogRequests - Adaptor to log the request being processed
func LogRequests(logger *log.Logger) Adaptor {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			logger.Printf("Processing request: %+v", r)
			defer logger.Printf("Finished request: %+v", w.Header())
			h.ServeHTTP(w, r)
		})
	}
}

// Adapt - Adapts the provided http.Handler with the given Adaptors
func Adapt(h http.Handler, adaptors ...Adaptor) http.Handler {
	for i := len(adaptors) - 1; i >= 0; i-- {
		h = adaptors[i](h)
	}
	return h
}

// GetBasicTemplateHandler - Returns a handler writes output from the template exetuted with name tmpl passed in
func GetBasicTemplateHandler(templates *template.Template, tmpl string, data interface{}) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		templates.ExecuteTemplate(w, tmpl, data)
	})
}

// GetTemplateEmbedder - Return a function which evaluates a template of the provided name
//
// This can be used to allow dynamic template evocation
func GetTemplateEmbedder(templates *template.Template) func(string, interface{}) (interface{}, error) {
	return func(name string, data interface{}) (interface{}, error) {
		var buf bytes.Buffer
		err := templates.ExecuteTemplate(&buf, name, data)
		return template.HTML(buf.String()), err
	}
}

// Handle - Handle the provided endpoint with the given handler
func Handle(ep string, handler http.Handler) {
	// This may do more fancy stuff in future
	http.Handle(ep, handler)
}

// ListenAndServe - Start the server
func ListenAndServe(logger *log.Logger, addr string, handler http.Handler, launchBrowser bool) {
	logger.Printf("Starting to listen on [%v]", addr)
	l, err := net.Listen("tcp", addr)
	if err != nil {
		logger.Fatalf("Unable to listen on [%v]: %v", addr, err)
		return
	}

	if launchBrowser {
		utils.OpenBrowser("http://" + addr)
	}

	logger.Printf("Starting to serve...")
	http.Serve(l, handler)
}
