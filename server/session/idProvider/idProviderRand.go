package idProvider

import (
	"crypto/rand"
	"encoding/base64"

	"gitlab.com/FluffGlitter/BobsMods/utils"
)

const providerName string = "IdProviderRand"

// randProvider - Random number based session ID provider
type randProvider struct {
	// Nothing to see here
}

// NextID - Get the next session ID
func (provider *randProvider) NextID() (interface{}, error) {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return nil, utils.Errorf("Unable to create new session ID")
	}

	return base64.URLEncoding.EncodeToString(b), nil
}

// Init - Initialize the session ID provider with the given options
func (provider *randProvider) Init(opts map[string]interface{}) error {
	// No options required
	return nil
}

func init() {
	provider := &randProvider{}
	RegisterProvider(providerName, provider)
}
