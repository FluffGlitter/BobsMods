package idProvider

import "gitlab.com/FluffGlitter/BobsMods/utils"

// IDProvider - Interface to a session ID provider
type IDProvider interface {
	NextID() (interface{}, error)
	Init(opts map[string]interface{}) error
}

var providers map[string]IDProvider

func init() {
	providers = make(map[string]IDProvider)
}

// RegisterProvider - Register a session ID provider. Called from provider
// implementation init function to register the provider
func RegisterProvider(name string, provider IDProvider) error {
	_, exists := providers[name]
	if exists {
		return utils.Errorf("Provider exists [%v]", name)
	}

	providers[name] = provider
	return nil
}

// GetProvider - Return the provider registered against the given name
func GetProvider(name string) (IDProvider, error) {
	provider, exists := providers[name]
	if !exists {
		return nil, utils.Errorf("No such provider [%v]", name)
	}
	return provider, nil
}
