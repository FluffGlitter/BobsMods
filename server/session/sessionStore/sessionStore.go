package sessionStore

import "gitlab.com/FluffGlitter/BobsMods/utils"

// Storer - Stores session data in long term storage
type Storer interface {
	LoadSession(interface{}) (map[interface{}]interface{}, error)
	StoreSession(interface{}, map[interface{}]interface{}) error
	DeleteSession(interface{}) error
	Init(opts map[string]interface{}) error
}

var storers map[string]Storer

func init() {
	storers = make(map[string]Storer)
}

// registerStorer - Registers a session Storer for use by a server
func registerStorer(name string, storer Storer) error {
	if storers[name] != nil {
		return utils.Errorf("Storer exists [%v]", name)
	}

	storers[name] = storer
	return nil
}

// GetStorer - Retrieve the session Storer for the given name
func GetStorer(name string) (Storer, error) {
	if storers[name] == nil {
		return nil, utils.Errorf("No such storer [%v]", name)
	}
	return storers[name], nil
}
