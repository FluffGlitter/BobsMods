package sessionStore

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"reflect"

	"gitlab.com/FluffGlitter/BobsMods/utils"
)

const storageName string = "SessionStoreFile"

// fileSessionStore - A very shitty, simple session store
type fileSessionStore struct {
	// TODO: Add locking for concurrancy
	initialized bool
	filename    string
	sessions    map[interface{}]map[interface{}]interface{}
	logger      *log.Logger
}

// single instance
var storage *fileSessionStore

// LoadSession - return the session data for the given session
func (store *fileSessionStore) LoadSession(session interface{}) (map[interface{}]interface{}, error) {
	// Just in case we havent loaded sessions by the time we get here
	if store.sessions == nil {
		err := store.loadSessions()
		if err != nil {
			return nil, err
		}
	}

	if store.sessions[session] == nil {
		return nil, utils.Errorf("No such session [%v] in store [%v]", session, store.filename)
	}

	return store.sessions[session], nil
}

// StoreSession - Store the session data to the file store
func (store *fileSessionStore) StoreSession(session interface{}, vars map[interface{}]interface{}) error {
	// Just in case we havent loaded sessions by the time we get here
	if store.sessions == nil {
		err := store.loadSessions()
		if err != nil {
			return err
		}
	}

	store.sessions[session] = vars

	// write through
	err := store.storeSessions()
	if err != nil {
		return err
	}

	return nil
}

// DeleteSession - Remove a session from storage (e.g. after expire)
func (store *fileSessionStore) DeleteSession(session interface{}) error {
	_, exists := store.sessions[session]
	if !exists {
		return utils.Errorf("No such session [%v]", session)
	}

	delete(store.sessions, session)
	err := store.storeSessions()
	return err
}

// Init - Initialize the file storage. Used to set configurable options before use
func (store *fileSessionStore) Init(opts map[string]interface{}) error {
	filename, exists := opts["Filename"].(string)
	if !exists || filename == "" {
		return utils.Errorf("Required parameter [Filename] not specified")
	}
	store.filename = filename

	store.initialized = true
	return nil
}

func stringify(i interface{}) string {
	switch i.(type) {
	case string:
		return i.(string)
	case fmt.Stringer:
		return i.(fmt.Stringer).String()
	default:
		// TODO: Handle other types
		return ""
	}
}

func jsonEncodeMap(m map[interface{}]interface{}) map[string]interface{} {
	ret := make(map[string]interface{})
	for key, value := range m {
		skey := stringify(key)
		if skey == "" {
			log.Printf("Unable to convert key type [%v] to string for value [%v]", reflect.TypeOf(key), value)
			continue
		}
		nvalue, ok := value.(map[interface{}]interface{})
		if ok {
			svalue := jsonEncodeMap(nvalue)
			ret[skey] = svalue
		} else {
			ret[skey] = value
		}
	}
	return ret
}

func jsonDecodeMap(m map[string]interface{}) map[interface{}]interface{} {
	ret := make(map[interface{}]interface{})
	for key, value := range m {
		ikey := interface{}(key)
		ret[ikey] = value
	}
	return ret
}

func (store *fileSessionStore) getJSessions() map[string]interface{} {
	m := make(map[interface{}]interface{})
	for key, value := range store.sessions {
		ivalue := interface{}(value)
		m[key] = ivalue
	}
	return jsonEncodeMap(m)
}

func (store *fileSessionStore) putJSessions(jsessions map[string]interface{}) {
	m := make(map[interface{}]map[interface{}]interface{})
	for key, value := range jsessions {
		var ikey interface{} = key
		mvalue := value.(map[string]interface{})
		m[ikey] = jsonDecodeMap(mvalue)
	}
	store.sessions = m
}

// loadSessions - internal function to load session data from file
func (store *fileSessionStore) loadSessions() error {
	if store.initialized == false {
		return utils.Errorf("Session storage not initialized")
	}

	file, err := ioutil.ReadFile(store.filename)
	if err != nil {
		// assume sessions have never been saved. (Do this better in a future backend)
		return nil
	}

	jsessions := make(map[string]interface{})
	err = json.Unmarshal(file, &jsessions)
	if err != nil {
		return utils.Errorf("Invalid session store data [%v] (%v): %v", store.filename, file, err)
	}
	store.putJSessions(jsessions)

	return nil
}

// storeSessions - internal function to store session data to file
func (store *fileSessionStore) storeSessions() error {
	if store.initialized == false {
		return utils.Errorf("Session storage not initialized")
	}

	jsessions := store.getJSessions()
	data, err := json.Marshal(jsessions)
	if err != nil {
		return utils.Errorf("Unable to convert session data for store [%v]: %v", store.filename, err)
	}

	err = ioutil.WriteFile(store.filename, data, 0644)
	if err != nil {
		return utils.Errorf("Unable to store session data [%v]: %v", store.filename, err)
	}

	return nil
}

func init() {
	storage = &fileSessionStore{initialized: false}
	err := registerStorer(storageName, storage)
	if err != nil {
		log.Fatalf("Unable to register storage [%v]: %v", storageName, err)
	}
}
