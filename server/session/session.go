package session

import (
	"gitlab.com/FluffGlitter/BobsMods/server/session/idProvider"
	"gitlab.com/FluffGlitter/BobsMods/server/session/sessionStore"
	"gitlab.com/FluffGlitter/BobsMods/utils"
)

// Session - Session variables storage.
//
// Should be created via a SessionManager
type Session struct {
	id   interface{}
	vars map[interface{}]interface{}
}

// Get - Return a session variable with the given key
func (session *Session) Get(key interface{}) interface{} {
	val, exists := session.vars[key]
	if !exists {
		return nil
	}
	return val
}

// Set - Set a session variable of the given key to the given value
func (session *Session) Set(key interface{}, val interface{}) {
	session.vars[key] = val
}

// Delete - Remove a session variable with the given key
func (session *Session) Delete(key interface{}) {
	delete(session.vars, key)
}

// ID - return the ID associated with this session
func (session *Session) ID() interface{} {
	return session.id
}

// Manager - Manages sessions via a SessionStorer
type Manager struct {
	// TODO: Add locking for concurrancy
	storage  sessionStore.Storer
	sessions map[interface{}]*Session
	id       idProvider.IDProvider
}

// GetSession - Returns a session from the sesion storage
func (manager *Manager) GetSession(id interface{}) (*Session, error) {
	var err error
	session, exists := manager.sessions[id]
	if !exists {
		session = &Session{id: id}
		// Not currently in the cache, try loading from storage
		session.vars, err = manager.storage.LoadSession(id)
		if err != nil {
			// Not in storage. Error
			return nil, utils.Errorf("No such session [%v]", id)
		}
		// Store it in the cache for future requests
		manager.sessions[id] = session
	}
	return session, nil
}

// NewSession - Create a new session
func (manager *Manager) NewSession() (*Session, error) {
	id, err := manager.id.NextID()
	if err != nil {
		return nil, err
	}

	session := &Session{id: id}
	session.vars = map[interface{}]interface{}{}
	manager.sessions[id] = session

	return manager.GetSession(id)
}

// PutSession - Indicates that the session is finished with and it can be saved
// to storage if the storage privider desires
func (manager *Manager) PutSession(session *Session) error {
	err := manager.storage.StoreSession(session.id, session.vars)
	return err
}

// DeleteSession - Remove a session (e.g. after expire)
func (manager *Manager) DeleteSession(session *Session) error {
	return manager.storage.DeleteSession(session.id)
}

// NewManager - Create a new session manager
func NewManager(opts map[string]interface{}) (*Manager, error) {
	var err error

	storage, exists := opts["SessionStorage"].(string)
	if !exists {
		return nil, utils.Errorf("opts must specify \"storage\"")
	}
	storageOpts, exists := opts["SessionStorageOpts"].(map[string]interface{})
	if !exists {
		storageOpts = make(map[string]interface{})
	}

	idp, exists := opts["IdProvider"].(string)
	if !exists {
		return nil, utils.Errorf("opts must specify \"idProvider\"")
	}
	idpOpts, exists := opts["IdProviderOpts"].(map[string]interface{})
	if !exists {
		idpOpts = make(map[string]interface{})
	}

	manager := &Manager{}
	manager.storage, err = sessionStore.GetStorer(storage)
	if err != nil {
		return nil, err
	}
	err = manager.storage.Init(storageOpts)
	if err != nil {
		return nil, err
	}

	manager.id, err = idProvider.GetProvider(idp)
	if err != nil {
		return nil, err
	}
	err = manager.id.Init(idpOpts)
	if err != nil {
		return nil, err
	}

	manager.sessions = make(map[interface{}]*Session)

	return manager, nil
}
